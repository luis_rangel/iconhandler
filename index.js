var lwip = require('lwip');
var mkdirp = require('mkdirp');

var model = {
	"AppIcon-1.appiconset" : [
		22
	],
	"AppIcon.appiconset" : [
		29,
		40,
		60
	]
};

function createFile(directory, sideSize, sufix, multiplier) {
	var newIconFile = directory + "/AppIcon" + sideSize + "x" + sideSize + sufix + ".png";
	lwip.open('image.png', function(err, image) {
		if(err) return console.log(err);
		image.batch().resize(sideSize * multiplier, sideSize * multiplier).writeFile(newIconFile, function (err) {
			if(err) {
				console.log(err);
			}  else {
				console.log("Created: " + newIconFile);
			}
		});
	});
}


function processDirectory(directory) {
	mkdirp(directory, function (err) {
		if(err) {
			console.log(err);
		} else {
			var expectedClones = model[directory];
			for (var sizeIndex = 0; sizeIndex < expectedClones.length; sizeIndex++) {
				var size = expectedClones[sizeIndex];
				createFile(directory, size, "", 1);
				createFile(directory, size, "@2x", 2);
				createFile(directory, size, "@3x", 3);
			}
		}
	});
}
for (directory in model) {
	processDirectory(directory);
}
